﻿/* MAIN */
  Develop & Design: Sebastián Romero
  Twitter: @sebastianromero
  Site: http://www.sebastianromerog.com

/* SITE */
  Doctype: HTML5

/* TOOLS */
  Sublime Text
  PHP Storm
  Gulp
  BrowserSync
  SCSS
  Wordpress

/* SPECIAL THANKS */
  Designer: Melisa Navarro 
  Site: http://www.melisanavarro.com
