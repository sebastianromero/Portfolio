var gulp        = require('gulp');
var sass        = require('gulp-sass');
var filter      = require('gulp-filter');
var sourcemaps  = require('gulp-sourcemaps');
var prefix = require('gulp-autoprefixer');
var minifyCSS   = require('gulp-minify-css');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var bourbon = require('node-bourbon');

gulp.task('sass', function() {
    gulp.src(['./scss/*scss', 'scss/**/*.scss'])
        .pipe(sass({
            includePaths: require('node-neat').includePaths
        }))
        //.pipe(sourcemaps.init())
        .pipe(prefix("last 1 version", "> 1%", "ie 8", "ie 7"))
        //.pipe(sourcemaps.write('/.'))
        .pipe(minifyCSS(['keepSpecialComments: *', 'benchmark']))
        .pipe(gulp.dest('./'))
        .pipe(filter('**/*.css')) 
        .pipe(browserSync.reload({stream:true}));
});

gulp.task('browser-sync', function() {
    browserSync({
        proxy: "192.168.1.4/wp",
    });

    gulp.watch(['scss/**/*.scss', 'scss/*.scss'], ['sass'])
    gulp.watch(['./*.php'], reload)

});



gulp.task('default', ['sass', 'browser-sync'], function () {

});