<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<script type="text/javascript">
		WebFontConfig = {
			google: { families: [ 'Roboto:500,300,500italic,300italic:latin' ] }
		};
		(function() {
			var wf = document.createElement('script');
			wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
			'://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
			wf.type = 'text/javascript';
			wf.async = 'true';
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(wf, s);
		})(); </script>
	<meta charset = "<?php bloginfo( 'charset' ); ?>">
	<meta name = "viewport" content = "width=device-width, initial-scale=1">
	<title><?php wp_title(); ?></title>
	<link rel = "shortcut icon" href = "<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico"/>
	<link rel = "profile" href = "http://gmpg.org/xfn/11">
	<link rel = "pingback" href = "<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div itemscope itemtype = "http://schema.org/LocalBusiness" class = "contenedor">
	<header class = "cabecera" role = "banner">
		<div class = "sitio-nombre">
			<a class = "sitio-titulo" href = "<?php echo esc_url( home_url( '/' ) ); ?>"
			   rel = "home"><span itemprop = "name"><?php bloginfo( 'name' ); ?></span></a>
		</div>
		<nav class = "menu" role = "navigation">
			<?php wp_nav_menu(); ?>
		</nav>
	</header>
	<div id = "contenido" class = "sitio-contenido">
		<?php //PORTFOLIO ?>

		<?php if ( is_front_page() && is_home() ) { ?>
		<?php } elseif ( is_front_page() ) { ?>
			<ul class = "p__grilla">
				<?php
					$my_query = new WP_Query( '&posts_per_page=-1' );
					while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
<li class = "p__proyecto">
	<a class = "p__link" href = "<?php the_permalink(); ?>">
		<figure class = "p__contenido">
			<div class="p__foto-wrap"> <?php the_post_thumbnail( 'thumbnail', array( 'class' => 'p__foto' ) ); ?></div>
		<figcaption class="p__meta-descripcion">
			<?php the_excerpt(); ?>
		</figcaption>
		</figure>
		<div class = "p__meta">
			<h1 class = "p__meta-titulo"> <?php the_title(); ?></h1>
			<div class="p__meta-categorias">
			<?php
				$categorias = wp_get_object_terms( $post->ID,  'proyectos' );
				if ( ! empty( $categorias ) ) {
					if ( ! is_wp_error( $categorias ) ) {
						echo '<ul class="categorias-lista">';
						foreach( $categorias as $term ) {
							echo '<li>' . $term->name . '</li>';
						}
						echo '</ul>';
					}
				}
			?>
			</div>
		</div>
	</a>
</li>
					<?php endwhile;
					wp_reset_query(); ?>
</ul>
			<?php //BLOG ?>
		<?php } elseif ( is_home() ) { ?>
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>
<article id = "post-<?php the_ID(); ?>" <?php post_class( "articulo" ); ?> >
	<div class = "post-wrap">
		<div class = "post__info">
			<h1 class = "articulo__titulo">
				<a href = "<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</h1>

			<h2 class = "articulo__fecha"><?php the_time( 'j \d\e\ F \d\e\ Y' ); ?></h2>
		</div>
		<div class = "post__contenido">
			<?php the_content(); ?>
		</div>
	</div>
</article>
				<?php endwhile; ?>
				<div class = "nav">
					<div class = "nav-atras">
						<?php next_posts_link( 'Atras' ); ?>
					</div>
					<div class = "nav-adelante">
						<?php previous_posts_link( 'Adelante' ); ?>
					</div>
				</div>
			<?php else : ?>
			<?php endif; ?>
		<?php }; ?>


<?php if ( is_single() ||  is_singular("proyecto") || ( is_page() ) ) { ?>

<?php if ( have_posts() ) : ?>

<?php while ( have_posts() ) : the_post(); ?>
<article id = "post-<?php the_ID(); ?>" <?php post_class( "articulo" ); ?>>
	<div class = "post-wrap">
		<?php if ( ! is_page()) { ?>
		<div class = "post__info">
			<h1 class = "articulo__titulo">
				<a href = "<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>

			<h2 class = "articulo__fecha"><?php the_time( 'j \d\e\ F \d\e\ Y' ); ?>    </h2>
		</div>
		<div class = "post__contenido">
			<?php }; ?>
			<?php if (is_page()) { ?>
			<div class = "post__contenido-sintitulo">
				<?php }; ?>
				<?php the_content(); ?>
			</div>
		</div>
	</div>
</article>


<?php if ( is_single() && !( is_singular("proyecto")) && !( is_page() ) ) { ?>
<div class = "nav">
	<div class = "nav-atras">
		<?php previous_post_link( '<span>Post anterior</span> </br>%link' ); ?>
	</div>
	<div class = "nav-adelante">
		<?php next_post_link( '<span>Post siguiente</span></br>%link' ); ?>
	</div>
</div>
<?php }; ?>


<?php if ( is_singular("proyecto")  ) { ?>
<div class = "nav">
	<div class = "nav-atras">
		<?php previous_post_link( '<span>Proyecto anterior </span> </br>%link' ); ?>
	</div>
	<div class = "nav-adelante">
		<?php next_post_link( '<span>Proyecto siguiente</span> </br>%link' ); ?>
	</div>
</div>
<?php }; ?>




<?php endwhile; ?>

<?php else : ?>

<?php endif; ?>

<?php }; ?>





		<?php //ERROR 404 ?>
		<?php if ( is_404() ) { ?>
<article>
	<p>Creo que acá no hay nada. <a href = "<?php echo esc_url( home_url( '/' ) ); ?>"> Mejor volvamos.</a></p>
</article>
		<?php }; ?>



	</div>
</div>
<?php wp_footer(); ?>
</body>
</html>