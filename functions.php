<?php
/**
 * Portfolio functions and definitions
 *
 * @package Portfolio
 */


// Register Style
function estilos() {

	if( is_user_logged_in() ) {
		
	wp_register_script( 'livereload', '//localhost:35729/livereload.js', false, false, true );
	wp_enqueue_script( 'livereload' );
		};

	wp_register_script( 'theme', get_template_directory_uri() . '/js/theme.js', false, false, true );
	wp_enqueue_script( 'theme' );

	wp_register_script( 'analytics', get_template_directory_uri() . '/js/analytics.js', false, false, false );
	wp_enqueue_script( 'analytics' );


	wp_register_style( 'style', get_template_directory_uri() . '/style.css', false, false );
	wp_enqueue_style( 'style' );

}

// Hook into the 'wp_enqueue_scripts' action
add_action( 'wp_enqueue_scripts', 'estilos' );

// Register Navigation Menus
function menu() {

	$locations = array(
		'menu' => 'Menu principal',
	);
	register_nav_menus( $locations );

}

// Hook into the 'init' action
add_action( 'init', 'menu' );



function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

add_theme_support( 'post-thumbnails' );




?>